module.exports = {
    mode: "development",
    //Set of rules to follow : 
    module: {
        rules: [
            {
                test:/\.js$/,
                exclude: /node_modules/,
                use:{
                    loader : 'babel-loader'
                }
            },
            {
                test:/\.css$/,
                exclude: /node_modules/,
                use : ['style-loader' , 'css-loader']
            }
        ]
    },
    devtool : 'source-map',
    devServer : {
        static : './dist/'
    }
}