// ---------------------------------------
import "./index.css";
import username from "./subscript2";
import userId from "./subscript1";
// --------------------------------------

const userInfoUL = document.querySelector('.user_info_ul');
const myUserArray = [username , userId];

for(let i = 0 ; i < 2 ; i++) {
    const li = document.createElement('li');
    li.innerText = myUserArray[i];
    userInfoUL.appendChild(li);
}

const obj = {
    a : 'alpha',
    b : 'beta',
    c : 'charlie'
};

const newObj = {
    ...obj,
    d: 'David'
};

console.log("----------------- OBJECT CONSOLING after Changes----------");
console.log(newObj);
console.log("-----------------------------------");